import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { CommentListComponent } from './comment-list/comment-list.component';
import { CommentItemComponent } from './comment-item/comment-item.component';
import { CommentFormComponent } from './comment-form/comment-form.component';
import { CommentsComponent } from './comments.component';
import { CommentsService } from './shared/comments.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule
  ],
  declarations: [
    CommentListComponent,
    CommentItemComponent,
    CommentFormComponent,
    CommentsComponent
  ],
  providers: [
    CommentsService
  ],
  exports: []
})
export class CommentsModule {}
