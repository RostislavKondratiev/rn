import { Component, OnDestroy, OnInit } from '@angular/core';
import { SessionService } from '../../../core/services/session.service';
import { AuthService } from '../../../core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'rn-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public username = 'Guest';
  public isLogin = false;
  private subscription;
  constructor(private session: SessionService, private authService: AuthService, private router: Router) {}
  public ngOnInit() {
    this.isLogin = this.session.isLogin();
    this.username = this.session.user || 'Guest';
    this.subscription = this.authService.userStateListener().subscribe((res) => {
      this.username = 'Guest';
      this.isLogin = !!res;
    });
  }
  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  public logOut() {
    this.authService.logOut();
  }
  public goToAuthorize() {
    this.router.navigate(['/authorization']);
  }
}
