import { Component, Input, OnChanges } from '@angular/core';
@Component({
  selector: 'rn-input-errors',
  templateUrl: './input-errors.component.html',
  styleUrls: ['./input-errors.component.scss']
})
export class InputErrorsComponent implements OnChanges {
  @Input() public form;
  @Input() public elem;
  @Input() public errors;
  public list = {
    required: 'This field is required',
    email: 'This field should be in email format',
    match: 'Passwords dont match',
    minlength: 'Password must contain at least 6 symbols',
    price: 'Price must be in "111.23" format',
    filetype: 'Invalid file type, .jpg and .png allowed'
  };
  public flag: boolean;
  private key: string[];
  public ngOnChanges() {
    if (this.errors) {
      this.key = Object.keys(this.errors);
      this.getMessage();
    } else {
      this.key = null;
      this.flag = false;
    }
  }
  public getMessage() {
    if (this.key) {
      this.flag = true;
      return this.list[`${this.key}`];
    }
  }
}
