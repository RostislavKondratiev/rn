import { Component, Input, OnInit } from '@angular/core';
import { ConfigService } from '../../core/services/config.service';
@Component({
  selector: 'rn-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  @Input() public item;
  constructor(private config: ConfigService) {}
  public ngOnInit() {
    this.item.img = `${this.config.imgPath + this.item.img}`;
    console.log(this.item);
  }
}
