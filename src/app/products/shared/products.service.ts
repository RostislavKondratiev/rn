import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../core/services/config.service';
import 'rxjs/add/operator/map';
import { Product } from '../../core/models/product.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProductsService {
  constructor(private http: HttpClient, private config: ConfigService) {}
  public getProducts(): Observable<Product[]> {
    return this.http.get(`${this.config.url}/products`).map((res: Response) => {
      if (Array.isArray(res)) {
        let tmp = [];
        res.forEach((item) => {
          tmp.push(new Product(item));
        });
        return tmp;
      }
    });
  }
}
