import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { SessionService } from '../core/services/session.service';

@Injectable()
export class AuthCanActivate implements CanActivate {
  constructor(
    private session: SessionService,
    private router: Router
  ) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.session.isLogin()) {
      if (state.url.split('/')[1] === 'authorization') {
        return false;
      }
      return true;
    } else if (state.url.split('/')[1] === 'authorization') {
      return true;
    } else { this.router.navigate(['products']); return false; }
  }
}
