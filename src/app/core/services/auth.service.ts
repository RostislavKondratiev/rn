import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { SessionService } from './session.service';
import { Credentials } from '../models/registration.model';
import { Subject } from 'rxjs/Subject';
import { AuthResponse } from '../models/auth-response';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  public userState = new Subject();
  constructor(private http: HttpClient,
              private config: ConfigService,
              private session: SessionService,
              private router: Router) {}
  public userStateListener() {
    return this.userState.asObservable();
  }
  public signUp(value) {
    let data = new Credentials(value);
    return this.http.post(`${this.config.url}/register/`, data);
  }
  public signIn(value) {
    let data = new Credentials(value);
    this.session.tempUsername = data.username;
    return this.http.post(`${this.config.url}/login/`, data).map((res: Response) => {
      let response = new AuthResponse(res);
      if (response.success) {
        this.session.user = this.session.tempUsername;
        this.session.token = response.token;
        this.session.tempUsername = null;
        this.router.navigate(['/products']);
      } else {
        this.session.tempUsername = null;
      }
      return res;
    });
  }
  public logOut() {
    this.session.user = '';
    this.session.token = null;
    this.userState.next(false);
  }
}
