import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SessionService } from './session.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private session: SessionService) {}
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const token = this.session.token;
      if (!!token) {
        const authReq = req.clone({
          headers: req.headers.set('Authorization', `Token ${this.session.token}`)
        });
        return next.handle(authReq);
      } else {
        return next.handle(req);
      }
  }
}
