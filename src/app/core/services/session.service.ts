import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class SessionService {
  constructor(private cookie: CookieService) {}
  public set tempUsername(data) {
    localStorage.setItem('temp', data);
  }
  public get tempUsername() {
    return localStorage.getItem('temp');
  }
  public set user(data) {
    localStorage.setItem('user', data);
  }
  public get user() {
    return localStorage.getItem('user');
  }
  public set token(data) {
    if (data) {
      this.cookie.put('token', data);
    } else {
      this.cookie.remove('token');
    }
  }
  public get token() {
    return this.cookie.get('token');
  }
  public isLogin() {
    return !!(this.user && this.token);
  }
}
