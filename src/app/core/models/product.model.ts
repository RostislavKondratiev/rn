export class Product {
  public id: number;
  public img: string;
  public text: string;
  public title: string;
  constructor(data) {
    this.id = data.id;
    this.img = data.img;
    this.text = data.text;
    this.title = data.title;
  }
}
