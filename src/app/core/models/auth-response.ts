export class AuthResponse {
  public success: boolean;
  public token: string;
  constructor(data) {
    this.success = data.success;
    this.token = data.token;
  }
}
