import { Routes } from '@angular/router';
import { AuthWrapComponent } from './auth-wrap/auth-wrap.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthCanActivate } from '../guards/auth.can-activate.guard';

export const ROUTES: Routes = [
  {path: 'authorization', component: AuthWrapComponent, canActivate: [AuthCanActivate], children: [
    {path: '', redirectTo: 'sign-in', pathMatch: 'full'},
    {path: 'sign-in', component: SignInComponent},
    {path: 'sign-up', component: SignUpComponent}
  ]}
];
