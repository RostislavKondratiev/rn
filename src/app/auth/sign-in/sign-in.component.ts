import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SessionService } from '../../core/services/session.service';
import { AuthResponse } from '../../core/models/auth-response';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'rn-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent {
  public signInForm = new FormGroup({
    email: new FormControl(null),
    password: new FormControl(null)
  });
  constructor(private authService: AuthService, private session: SessionService, private router: Router) {}
  public submit($event, form) {
    $event.preventDefault();
    this.authService.signIn(form.value).subscribe((res) => {

    });
  }
  public goBack() {
    this.router.navigate(['/products']);
  }
}
