import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ROUTES } from './auth.routing';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { AuthComponent } from './auth.component';
import { AuthPageService } from './shared/auth-page.service';
import { AuthWrapComponent } from './auth-wrap/auth-wrap.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [
    SignUpComponent,
    SignInComponent,
    AuthComponent,
    AuthWrapComponent,
  ],
  providers: [AuthPageService],
  exports: [RouterModule]
})
export class AuthModule {}
