import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'rn-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent {
  public signUpForm = new FormGroup({
    email: new FormControl(null),
    password: new FormControl(null),
    confirm: new FormControl(null)
  });
  constructor(private authService: AuthService, private router: Router) {}
  public submit($event, form) {
    $event.preventDefault();
    this.authService.signUp(form.value).subscribe();
  }
  public goBack() {
    this.router.navigate(['/products']);
  }
}
