import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../core/models/product.model';

@Component({
  selector: 'rn-product-description',
  templateUrl: './product-description.component.html',
  styleUrls: ['./product-description.component.scss']
})
export class ProductDescriptionComponent implements OnInit {
  public product: Product;
  constructor(private route: ActivatedRoute) {}
  public ngOnInit () {
    this.route.data.subscribe((res) => {
      this.product = res.product;
      console.log(this.product);
    });
  }
}
