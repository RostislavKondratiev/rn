import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductDescriptionComponent } from './product-description.component';
import { RouterModule } from '@angular/router';
import { ROUTES } from './product-description.routing';
import { ProductDescriptionService } from './shared/product-description.service';
import { ProductDescriptionResolver } from './shared/product-description.resolver';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [
    ProductDetailsComponent,
    ProductDescriptionComponent
  ],
  providers: [
    ProductDescriptionService,
    ProductDescriptionResolver
  ],
  exports: [RouterModule]
})
export class ProductDescriptionModule {
}
