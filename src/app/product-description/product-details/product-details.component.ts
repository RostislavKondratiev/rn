import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../core/models/product.model';
import { ConfigService } from '../../core/services/config.service';

@Component({
  selector: 'rn-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit{
  @Input() public product: Product;
  constructor(private config: ConfigService) {}
  public ngOnInit() {
    this.product.img = `${this.config.imgPath}${this.product.img}`;
  }
}
