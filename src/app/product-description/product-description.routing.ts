import { Routes } from '@angular/router';
import { ProductDescriptionComponent } from './product-description.component';
import { ProductDescriptionResolver } from './shared/product-description.resolver';

export const ROUTES: Routes = [
  { path: '', pathMatch: 'full', component: ProductDescriptionComponent, resolve: {product: ProductDescriptionResolver}}
];
